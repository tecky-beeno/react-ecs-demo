import React, { useEffect } from 'react'

export default function Timer(props: {
  duration: number
  interval: number
  onTimePassed: (passedTime: number) => void
  onComplete: () => void
}) {
  const { duration, onComplete, interval, onTimePassed } = props

  useEffect(() => {
    let timer = setTimeout(onComplete, duration)
    return () => clearTimeout(timer)
  }, [duration, onComplete])

  useEffect(() => {
    let startTime = Date.now()
    let timer = setInterval(
      () => onTimePassed(Date.now() - startTime),
      interval,
    )

    return () => clearInterval(timer)
  }, [interval, onTimePassed])

  return <></>
}
