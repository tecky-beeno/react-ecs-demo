import { symlinkSync } from 'fs'
import React, { useCallback, useEffect, useState } from 'react'
import './App.css'
import Timer from './components/Timer'
import logo from './logo.svg'

function App() {
  const [hasEnd, setHasEnd] = useState(false)
  const [timePassed, setTimePassed] = useState(0)
  const [level, setLevel] = useState(1)

  const onComplete = useCallback(() => setHasEnd(true), [])
  const onTimePassed = useCallback((passedTime: number) => {
    setTimePassed(passedTime)
    setLevel(l => l + 1)
  }, [])

  return (
    <div className="App">
      <Timer
        duration={5000}
        onComplete={onComplete}
        interval={1000}
        onTimePassed={onTimePassed}
      />
      <p>hasEnd: {hasEnd ? 'yes' : 'no'}</p>
      <p>timePassed: {timePassed}</p>
      <p>level: {level}</p>
      <GameWorld />
    </div>
  )
}

class Point {
  constructor(public x: number, public y: number) {}
}

class Tomato {
  constructor(public position: Point, public speed: Point) {}

  render(context: CanvasRenderingContext2D, image: HTMLImageElement) {
    context.drawImage(image, this.position.x, this.position.y)
  }

  update(delta: number) {
    this.position.x += this.speed.x * delta
    this.position.y += this.speed.y * delta
  }
}

let gameEngine = {
  entities: {
    good: new Tomato(new Point(0, 0), new Point(30, 30)),
    bad: new Tomato(new Point(350, 0), new Point(-30, 30)),
  },
  systems: {
    time: {
      last: Date.now(),
      getDelta: function () {
        let now = Date.now()
        let delta = now - this.last
        this.last = now
        return delta
      },
    },
    physic: {
      update: () => {
        let delta = gameEngine.systems.time.getDelta() / 1000

        gameEngine.entities.good.update(delta)
        gameEngine.entities.bad.update(delta)
      },
    },
  },
}

function useImage(src: string) {
  const [image, setImage] = useState<HTMLImageElement | null>(null)
  useEffect(() => {
    if (image != null) {
      return
    }
    let img = new Image()
    img.onload = () => setImage(img)
    img.src = src
  }, [image, src])
  return image
}

function GameWorld() {
  const [canvas, setCanvas] = useState<HTMLCanvasElement | null>(null)
  const [context, setContext] = useState<CanvasRenderingContext2D | null>(null)
  const goodImage = useImage('/good.png')
  const badImage = useImage('/bad.png')

  function init(canvas: HTMLCanvasElement | null) {
    if (!canvas) {
      return
    }
    const context = canvas.getContext('2d')
    if (!context) {
      return
    }
    setContext(context)
  }

  useEffect(() => {
    init(canvas)
  }, [canvas])

  useEffect(() => {
    if (!context || !goodImage || !badImage) {
      return
    }
    let timer = setInterval(function draw() {
      context.clearRect(0, 0, 400, 400)

      context.beginPath()
      context.fillStyle = 'red'
      context.strokeStyle = '1px solid green'
      context.rect(10, 10, 30, 30)
      context.stroke()

      gameEngine.entities.good.render(context, goodImage)
      gameEngine.entities.bad.render(context, badImage)

      gameEngine.systems.physic.update()
    }, 1000 / 30)
    return () => {
      clearInterval(timer)
    }
  }, [context, goodImage, badImage])

  return (
    <>
      game world
      <div
        style={{ width: '400px', height: '400px', outline: '1px solid black' }}
      >
        <canvas width={400} height={400} ref={e => setCanvas(e)}></canvas>
      </div>
    </>
  )
}

export default App
